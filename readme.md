If you are watching the Udemy video for this project, you will need to update your
LoginForm.js file.  In video 123, he creates an error message for a failed login in attempt.
Originally, he has 

const mapStateToProps = (state) => {
 console.log(state.auth.error);
 return {
 email: state.auth.email,
 password: state.auth.password,
 error: state.auth.error
 };
};

However, a video or two later, the LoginForm.js file now reads as

const mapStateToProps = ({ auth }) => {
 const { email, password, error, loading } = auth;
 
 return { email, password, error, loading };
};

I hope no one has to spend 2 hours of their life questioning themselves about this error.