import React from 'react';
import { Text, TouchableOpacity } from 'react-native';


const Button = ({ onPress, children }) => { /*children must be used, can't use buttonText*/
    const { buttonStyle, textStyle } = styles;

    return (
        <TouchableOpacity
            style={buttonStyle}
            onPress={onPress}
        >
            <Text 
                style={textStyle}
            >{children}</Text>
        </TouchableOpacity>
    );
};

const styles = {
    buttonStyle: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft: 5,
        marginRight: 5,

    },
    textStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10,
        textAlign:'center',
    },
};

/*make the component availab to other parts of the app.  Notice we didn't use export default
since we are export an object which contains a key and value which are the same we can simplify 
it by just writing { Button } instead of { Button: Button}
*/
export { Button };