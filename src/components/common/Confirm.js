import React from 'react';
import { Text, View, Modal } from 'react-native';
import { CardSection } from './CardSection';
import { Button } from './Button';

const Confirm = ({ children, visible, onAccept, onDecline }) => {
    const { containerStyle, cardSectionStyle, textStyle } = styles;
    return (
        <Modal
            
            animationType="slide"
            onRequestClose={() => {}}
            //This is required for android, you can pass along an empty function
            transparent
            visible={visible}
        >
            <View style={containerStyle}>
                <CardSection>
                    <Text style={textStyle}>{children}</Text>
                </CardSection>

                <CardSection style={cardSectionStyle}>
                    <Button
                        onPress={onAccept}
                        /*
                        Note:  Notice that we are not invoking the function onAccept immediately
                        we are simply passing a reference to the function 
                        */
                    >Yes</Button>
                    <Button
                        onPress={onDecline}
                    >FUCK NO!!!</Button>
                </CardSection>
            </View>
        </Modal>
    );
};

const styles = {
    cardSectionStyle: {
        justifyContent: 'center'
    },
    textStyle: {
        flex: 1,
        fontSize: 18,
        textAlign: 'center',
        lineHeight: 40, //how much space is placed between each line of text
    },
    containerStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.75)',
        position: 'relative',
        flex: 1,
        justifyContent: 'center',
    }
};

export { Confirm };
