import React, { Component } from 'react';
import { connect } from 'react-redux';
import { employeeUpdate, employeeCreate } from '../actions';
import { Card, CardSection, Button } from './common';
import EmployeeForm from './EmployeeForm'; 


class EmployeeCreate extends Component {

    onButtonPress() {
        const { name, phone, shift } = this.props;
        
        this.props.employeeCreate({ name, phone, shift: shift || 'Monday' });
    }

    render() {
        //console.log(this.props.employee);
        return (
            <Card>
                <EmployeeForm {...this.props} />
                <CardSection>
                    <Button
                            onPress={this.onButtonPress.bind(this)}
                    >Create

                    </Button>
                </CardSection>
            </Card>
        );
    }
}

const mapStateToProps = (state) => {
    const { name, phone, shift } = state.employeeForm;
    /*
    NOTE:  We have access to state.employeeForm because we assigned employeeForm in our 
    combinedReducers inside the index.js file located in the reducers folder
    */

    return { name, phone, shift };
};

export default connect(mapStateToProps, { 
    employeeUpdate, employeeCreate 
})(EmployeeCreate);
