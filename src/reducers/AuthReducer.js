import { EMAIL_CHANGED 
     , PASSWORD_CHANGED,  
     LOGIN_USER_SUCCESS,
     LOGIN_USER_FAIL,
     LOGIN_USER 

} from '../actions/types';

const INITIAL_STATE = { 
    email: '',
    password: '',
    user: null,
    error: '',
    loading: false,
    successMSG: 'still working yo'
};

export default (state = INITIAL_STATE, action) => {
    console.log(action);
    switch (action.type) {
        case EMAIL_CHANGED:
            return { ...state, email: action.payload };
            /**
            This reads as "make a new object, take all of the properties off of my existing state 
            object and throw them into the new object, then define the PROPERTY email and give it 
            the  VALUE of action.payload, then add it on top of what was ever on top of the state 
            object  " 
             */
        case PASSWORD_CHANGED:
            return { ...state, password: action.payload };
        case LOGIN_USER_SUCCESS:
            return { ...state, ...INITIAL_STATE, user: action.payload, loading: false, };
        case LOGIN_USER_FAIL:
            return { ...state, error: 'You fail', loading: false };
        case LOGIN_USER:
            return { ...state, loading: true, error: '' };
        default:
            return state;
    }
};


/*
No errors will be thrown if you import an incorrect variable, which will result in an 
'undefined case', which means the reducer will never pick up on the action.  The best
way to verify is to put a console log after the case

*/
