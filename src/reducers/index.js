import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import EmployeeFormReducer from './EmployeeFormReducer';
import EmployeeReducer from './EmployeeReducer';

export default combineReducers({
    auth: AuthReducer, 
    employeeForm: EmployeeFormReducer,
    employees: EmployeeReducer

});
/*
NOTE:  auth is and amployeeForm are both state(s)
*/
