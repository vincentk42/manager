import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import REDUXTHUNK from 'redux-thunk';
import reducers from './reducers';
//import LoginForm from './components/LoginForm';
import Router from './Router';

class App extends Component {
    componentWillMount() {
        const config = {
            apiKey: 'AIzaSyDTmWY0huXyf3cgz_Si9YA71iTjRMkVXxE',
            authDomain: 'manager-bbda2.firebaseapp.com',
            databaseURL: 'https://manager-bbda2.firebaseio.com',
            projectId: 'manager-bbda2',
            storageBucket: 'manager-bbda2.appspot.com',
            messagingSenderId: '428233368990'
          };
           
          firebase.initializeApp(config);
    } 
    render() {
        const store = createStore(reducers, {}, applyMiddleware(REDUXTHUNK));
        /*
        NOTE:  the second argument is for any initial state that we want to pass to
        our redux application.  
        the third argument is a store enchancer
        */
        return (
            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}

export default App;
